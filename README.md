These are the subtitles to the Soviet film "The Battle of Stalingrad" (1949). I
posted the movie on [Goblin Refuge](https://goblinrefuge.com/mediagoblin/u/deathsbreed/m/the-battle-of-stalingrad-1949).
The movie was made in the Soviet Union, so it should be public domain (that and
it's old as fuck).